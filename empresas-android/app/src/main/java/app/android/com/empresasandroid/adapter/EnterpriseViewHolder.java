package app.android.com.empresasandroid.adapter;

import android.graphics.Picture;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import app.android.com.empresasandroid.R;

/**
 * Created by gbcma on 09/07/2017.
 */

public class EnterpriseViewHolder extends RecyclerView.ViewHolder {

    final TextView name;
    final TextView city;
    final TextView enterprisseType;
    final ImageView picture;

        public EnterpriseViewHolder(View view) {
        super(view);
            name = (TextView) view.findViewById(R.id.textViewName);
            city = (TextView) view.findViewById(R.id.textViewEnterprisseType);
            enterprisseType = (TextView) view.findViewById(R.id.textViewCity);
            picture = (ImageView) view.findViewById(R.id.imageViewPic);


    }

}