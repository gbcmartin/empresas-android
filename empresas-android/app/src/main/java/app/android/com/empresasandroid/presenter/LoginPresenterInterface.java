package app.android.com.empresasandroid.presenter;

/**
 * Created by gbcma on 10/07/2017.
 */

public interface LoginPresenterInterface {

    void submitLogin(String email, String password);
    void onDestroy();

}
