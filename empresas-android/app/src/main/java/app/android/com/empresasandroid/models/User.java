package app.android.com.empresasandroid.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gbcma on 08/07/2017.
 */

public class User {

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User() {
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;


    }
}
