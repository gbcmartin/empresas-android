package app.android.com.empresasandroid.presenter;

/**
 * Created by gbcma on 10/07/2017.
 */

public interface HomePresenterInterface {

    void getEnterprises(String newText);
    void onDestroy();

}
