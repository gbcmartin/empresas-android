package app.android.com.empresasandroid.api;

import java.util.List;
import app.android.com.empresasandroid.models.Enterprise;
import app.android.com.empresasandroid.models.Enterprises;
import app.android.com.empresasandroid.models.User;
import app.android.com.empresasandroid.models.UserAuth;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by gbcma on 08/07/2017.
 */

public interface ApiInterface{

    @POST("users/auth/sign_in/")
    Call<User> loginUser(@Body User user);

    @GET("enterprises")
    Call<Enterprises> getEnterprise(@Header("access-token") String token, @Header("client")String client, @Header("uid") String uid, @Query("name") String name);

}
