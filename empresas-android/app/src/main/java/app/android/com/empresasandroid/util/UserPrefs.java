package app.android.com.empresasandroid.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by gbcma on 08/07/2017.
 */

public class UserPrefs {
    public static final String PREFS_NAME="app.android.com.empresasandroid.util.UserPrefs";
    public static SharedPreferences settings;
    public static SharedPreferences.Editor editor;

    public UserPrefs(Context ctx){
        if(settings == null){
            settings = ctx.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        }
        editor = settings.edit();

    }

}
