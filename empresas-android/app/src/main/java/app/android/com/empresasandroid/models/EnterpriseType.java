package app.android.com.empresasandroid.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gbcma on 08/07/2017.
 */

public class EnterpriseType {
    @SerializedName("id")
    private Integer id;
    @SerializedName("enterprise_type_name")
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EnterpriseType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
