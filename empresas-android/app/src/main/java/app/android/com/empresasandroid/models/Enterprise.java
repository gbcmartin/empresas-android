package app.android.com.empresasandroid.models;

import com.google.gson.annotations.SerializedName;

import butterknife.BindView;

/**
 * Created by gbcma on 08/07/2017.
 */

public class Enterprise {

    @SerializedName("enterprise_name")
    private String name;

    @SerializedName("city")
    private String city;

    @SerializedName("country")
    private String country;

    @SerializedName("description")
    private String description;

    @SerializedName("enterprise_type")
    private EnterpriseType enterpriseType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(EnterpriseType enterpriseType) {
        this.enterpriseType = enterpriseType;
    }

    public Enterprise(String name, String city, String country, String description, EnterpriseType enterpriseType) {
        this.name = name;
        this.city = city;
        this.country = country;
        this.description = description;
        this.enterpriseType = enterpriseType;
    }
}
