package app.android.com.empresasandroid.presenter;

/**
 * Created by gbcma on 10/07/2017.
 */

import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.List;

import app.android.com.empresasandroid.activities.HomeActivity;
import app.android.com.empresasandroid.activities.LoginActivity;
import app.android.com.empresasandroid.adapter.EnterpriseAdapter;
import app.android.com.empresasandroid.api.Api;
import app.android.com.empresasandroid.api.ApiInterface;
import app.android.com.empresasandroid.models.Enterprise;
import app.android.com.empresasandroid.models.Enterprises;
import app.android.com.empresasandroid.models.UserAuth;
import app.android.com.empresasandroid.util.UserPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter implements HomePresenterInterface {

    private ApiInterface apiInterface;
    private UserAuth userAuth;
    private List<Enterprise> enterpriseList;

    private HomeActivity homeActivity;

    public HomePresenter(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
        apiInterface = Api.getRetrofit().create(ApiInterface.class);

        Gson gson = new Gson();
        userAuth = gson.fromJson(UserPrefs.settings.getString("userData",""),UserAuth.class);

    }

    public void getEnterprises(String newText){
        Call callEnterprise = apiInterface.getEnterprise(userAuth.getToken(),userAuth.getClient(),userAuth.getUid(),newText);
        callEnterprise.enqueue(new Callback<Enterprises>() {
            @Override
            public void onResponse(Call<Enterprises> call, Response<Enterprises> response) {
                if(response.isSuccessful()){
                    enterpriseList = response.body().getEnterprises();
                    homeActivity.recyclerView.setAdapter(new EnterpriseAdapter(enterpriseList, homeActivity.getApplicationContext() ));
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });
    }

    @Override
    public void onDestroy()
    {
        homeActivity = null;
    }
}
