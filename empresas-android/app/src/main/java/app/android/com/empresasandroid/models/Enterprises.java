package app.android.com.empresasandroid.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gbcma on 08/07/2017.
 */

public class Enterprises {

    @SerializedName("enterprises")
    List<Enterprise> enterprises;

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }
}
