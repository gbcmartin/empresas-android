package app.android.com.empresasandroid.activities;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;

import app.android.com.empresasandroid.R;
import app.android.com.empresasandroid.adapter.EnterpriseAdapter;
import app.android.com.empresasandroid.api.Api;
import app.android.com.empresasandroid.api.ApiInterface;
import app.android.com.empresasandroid.models.Enterprise;
import app.android.com.empresasandroid.models.Enterprises;
import app.android.com.empresasandroid.models.UserAuth;
import app.android.com.empresasandroid.presenter.HomePresenter;
import app.android.com.empresasandroid.util.UserPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    private TextView textViewStart;
    public RecyclerView recyclerView;
    public Toolbar toolbar;

    HomePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        textViewStart = (TextView) findViewById(R.id.textViewStart);
        setSupportActionBar(toolbar);

        RecyclerView.LayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layout);

        presenter = new HomePresenter(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewStart.setVisibility(View.INVISIBLE);
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                presenter.getEnterprises(newText);
                return true;
            }
        });

        return true;
    }

}
