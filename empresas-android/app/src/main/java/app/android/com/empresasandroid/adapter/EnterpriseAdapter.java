package app.android.com.empresasandroid.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import app.android.com.empresasandroid.R;
import app.android.com.empresasandroid.activities.DetailActivity;
import app.android.com.empresasandroid.activities.HomeActivity;
import app.android.com.empresasandroid.models.Enterprise;

/**
 * Created by gbcma on 09/07/2017.
 */

public class EnterpriseAdapter extends RecyclerView.Adapter {
    private List<Enterprise> enterprises;
    private Context context;

    public EnterpriseAdapter(List<Enterprise> enterprisse, Context context) {
        this.enterprises = enterprisse;
        this.context = context;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        EnterpriseViewHolder holder = (EnterpriseViewHolder) viewHolder;

        final Enterprise enterprise = enterprises.get(position);

        holder.name.setText(enterprise.getName());
        holder.enterprisseType.setText(enterprise.getEnterpriseType().getName());
        holder.city.setText(enterprise.getCity() + ", " + enterprise.getCountry() );
        holder.picture.setImageResource(R.drawable.img_e_1_lista);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentOpenDetailActivity = new Intent(context, DetailActivity.class);
                intentOpenDetailActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentOpenDetailActivity.putExtra("name",enterprise.getName());
                intentOpenDetailActivity.putExtra("description",enterprise.getDescription());
                context.startActivity(intentOpenDetailActivity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return enterprises.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.enterprisse_view_holder, parent, false);
        EnterpriseViewHolder holder = new EnterpriseViewHolder(view);
        return holder;
    }

}
