package app.android.com.empresasandroid.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import app.android.com.empresasandroid.R;
import app.android.com.empresasandroid.api.Api;
import app.android.com.empresasandroid.api.ApiInterface;
import app.android.com.empresasandroid.models.User;
import app.android.com.empresasandroid.models.UserAuth;
import app.android.com.empresasandroid.presenter.LoginPresenter;
import app.android.com.empresasandroid.util.UserPrefs;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.editTextEmail)
    EditText editTextEmail;

    @BindView(R.id.editTextPassword)
    EditText editTextPassword;

    @BindView(R.id.buttonLogin)
    Button buttonLogin;

    LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        presenter = new LoginPresenter(this);
    }

    @OnClick(R.id.buttonLogin)
    public void submit()
    {

        presenter.submitLogin(editTextEmail.getText().toString(), editTextPassword.getText().toString());
    }

    public void errorMessageUserPassword()
    {
        Toast.makeText(LoginActivity.this, "Prencha os campos E-mail e Senha", Toast.LENGTH_SHORT).show();
    }

    public void genericMessageError(Throwable t)
    {
        Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
    }

    public void errorInvalidLogin()
    {
        Toast.makeText(LoginActivity.this, "Login Invalido", Toast.LENGTH_SHORT).show();
    }

    public void callHomeActivity()
    {
        Intent intentOpenHomeActivity = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intentOpenHomeActivity);
    }

}
