package app.android.com.empresasandroid.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gbcma on 08/07/2017.
 */

 public class UserAuth {

    @SerializedName("access-token")
    private String token;

    @SerializedName("client")
    private String client;

    @SerializedName("uid")
    private String uid;

    public String getToken() { return token; }

    public void setToken(String token) {
        this.token = token;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public UserAuth(String token, String client, String uid) {
        this.token = token;
        this.client = client;
        this.uid = uid;
    }

}
