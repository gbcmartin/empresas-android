package app.android.com.empresasandroid.presenter;

import android.content.Intent;
import android.widget.Toast;
import com.google.gson.Gson;
import app.android.com.empresasandroid.activities.HomeActivity;
import app.android.com.empresasandroid.activities.LoginActivity;
import app.android.com.empresasandroid.api.Api;
import app.android.com.empresasandroid.api.ApiInterface;
import app.android.com.empresasandroid.models.User;
import app.android.com.empresasandroid.models.UserAuth;
import app.android.com.empresasandroid.util.UserPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gbcma on 10/07/2017.
 */

public class LoginPresenter implements LoginPresenterInterface {

    private ApiInterface apiInterface;
    private LoginActivity loginActivity;

    public LoginPresenter(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
        apiInterface = Api.getRetrofit().create(ApiInterface.class);

    }

    @Override
    public void submitLogin(String email, String password) {
        User user = new User(email, password);

        if(!email.isEmpty() || !password.isEmpty()) {
            Call callLogin = apiInterface.loginUser(user);
            callLogin.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.isSuccessful()) {
                        UserAuth userAuth = new UserAuth(response.headers().get("access-token"), response.headers().get("client"), response.headers().get("uid"));
                        UserPrefs prefs = new UserPrefs(loginActivity.getApplicationContext());
                        Gson gson = new Gson();
                        UserPrefs.editor.putString("userData", gson.toJson(userAuth));
                        UserPrefs.editor.commit();

                        if (loginActivity != null) {
                            loginActivity.callHomeActivity();
                        }
                    } else {
                        if (loginActivity != null) {
                            loginActivity.errorInvalidLogin();
                        }
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    if (loginActivity != null) {
                        loginActivity.genericMessageError(t);
                    }
                }
            });
        }
        else
        {
            if (loginActivity != null) {
                loginActivity.errorMessageUserPassword();
            }
        }
    }

    @Override public void onDestroy()
    {
        loginActivity = null;
    }
}
