package app.android.com.empresasandroid.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by gbcma on 08/07/2017.
 */

public class Api{

    private static String baseUrl = "http://54.94.179.135:8090";
    private static String version = "/api/v1/";

    public static Retrofit getRetrofit(){
        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(baseUrl + version)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

        return  retrofit;
    }
}
