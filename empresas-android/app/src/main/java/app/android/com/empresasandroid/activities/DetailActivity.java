package app.android.com.empresasandroid.activities;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import app.android.com.empresasandroid.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity {



    @BindView(R.id.textViewDescription)
    TextView textViewDescription;

    @BindView(R.id.imageViewPicture)
    ImageView imageViewPicture;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);
        textViewDescription.setText(getIntent().getStringExtra("description"));
        imageViewPicture.setImageResource(R.drawable.img_e_1);
        setTitle(getIntent().getStringExtra("name"));
        ab.setBackgroundDrawable(getDrawable(R.drawable.gradiente_color));
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
